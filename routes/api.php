<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'APIToken'], function () {
    Route::post('get_users', 'API\UserController@getUser');
    Route::post('add_student', 'API\StudentController@addStudent');
    Route::post('add_student_bulk', 'API\StudentController@uploadCsvData');
    Route::post('get_students', 'API\StudentController@getStudents');
    Route::post('send_mail', 'API\StudentController@sendMail');
    
    Route::post('get_all_classes', 'API\ClassController@getAllClasses');
    Route::post('add_class', 'API\ClassController@addClass');
    Route::post('update_class', 'API\ClassController@updateClass');
    Route::post('delete_class', 'API\ClassController@deleteClass');
    
    Route::post('add_subject', 'API\SubjectController@addSubject');
    Route::post('get_subject', 'API\SubjectController@getSubject');
    Route::post('update_subject', 'API\SubjectController@updateSubject');
    Route::post('delete_subject', 'API\SubjectController@deleteSubject');

    Route::post('add_material', 'API\MaterialController@addMaterail');
    Route::post('get_material', 'API\MaterialController@getMaterial');
    Route::post('update_material', 'API\MaterialController@updateMaterial');
    Route::post('delete_material', 'API\MaterialController@deleteMaterial');
});

Route::get('get_users', 'API\UserController@getUser');
Route::post('add_user', 'API\UserController@addUser');
Route::post('login', 'API\UserController@login');
