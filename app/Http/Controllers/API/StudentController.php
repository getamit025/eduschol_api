<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Student;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class StudentController extends Controller
{
    public function getStudents()
    {
       try {
            $student = Student::all();
            return $this->sendResultJSON(true,'',$student );    
       }
       catch (\Exception $e) {
        return $this->sendResultJSON(false, $e->getMessage());
        }
    }
    public function sendMail() {
        try {
            $hashed_random_password = Str::random(8);
             $details = [
                 'title' => 'Title: Test maiiil',
                 'body'  => 'Your password is :'.$hashed_random_password
             ];   
            Mail::to('shubhamgodbole30129@gmail.com')->send(new SendMail($details));
            
            return $this->sendResultJSON(false, 'send mail');
            // Mail::send(['text'=>'Mail'],['name','Shubham'],function($message){
            //     $message->to('shubhamgodbole30129@gmail.com')->subject("Email Testing with Laravel");
            //     $message->from('shubhamgodbole30129@gmail.com','Creative Losser Hopeless Genius');
            // });
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
            }
    
    }

    public function addStudent(Request $request)
    {
        try {
            $element_array = array(
                'first_name' => "required",
                'middle_name' => "required",
                'last_name' => "required",
                'gender' => "required",
                'dob' => "required",
                'religion' => "required",
                'class' => "required",
                'section' => "required",
                'roll_no' => "required",
                'email' => "required|email|unique:users,email",
                'mobile'=> "required",
                'address'=> "required",
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }
            $is_exist = student::where('email',$request->get('email'))->first();
            if($is_exist) {
                return $this->sendResultJSON(false,$request->get('email'). ' this email is alrady exist');                
            }
            $student = new Student();
            $student->first_name = $request->get('first_name');
            $student->middle_name = $request->get('middle_name');
            $student->last_name = $request->get('last_name');
            $student->gender = $request->get('gender');
            $student->dob = $request->get('dob');
            $student->religion = $request->get('religion');
            $student->class = $request->get('class');
            $student->section = $request->get('section');
            $student->roll_no = $request->get('roll_no');
            $student->email = $request->get('email');
            $student->mobile = $request->get('mobile');
            $student->address = $request->get('address');
            
            if($request->file('profile')) {
                $t=time();
                $file_name = $t."_profile.jpg";
                $path = $request->file('profile')->move(public_path("/image/"),$file_name);
                $file_uri = url('/image/'.$file_name);
                $student->profile = $file_uri;
//                return $this->sendResultJSON(true, 'Student created.',$file_uri);
            }
            $student->save();

            return $this->sendResultJSON(true, 'Student created.');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }


    public function uploadCsvData(Request $request)
    {
        ini_set('max_execution_time', '0');
        try {
            $element_array = array(
                'csv_file' => 'required|mimes:csv,txt',
            );

            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }
            
            $path = $request->file('csv_file')->getRealPath();
            
            $handle = fopen($path, 'r');
            $limit = 1000;

            $values = array();
            $titles = array();

            while (($data = fgetcsv($handle, $limit, ",")) !== FALSE) {
                if (count($data) > 1) {

                    $temp = array_map('trim', $data);
                    $temp2 = array();
                    foreach ($temp as $rawData) {
                        $temp2[] = $rawData;
                    }
                    
                    $temp3= [];
                    $temp3["name"] = $this->cleamCsvText($temp2[0]);
                    $temp3["photo"] = $this->cleamCsvText($temp2[1]) ;
                    $temp3["gender"] = $temp2[2];
                    $temp3["father_name"] =  $this->cleamMobile($temp2[3]) ;
                    $temp3["mother_name"] =  $this->cleamMobile($temp2[4]) ;
                    $temp3["dob"] = $this->cleamCsvText($temp2[5]);
                    $temp3["religion"] = $temp2[6];
                    $temp3["email"] = $temp2[7];
                    $temp3["addmission_date"] = $temp2[8];
                    $temp3["class"] = $temp2[9];
                    $temp3["section"] = $temp2[10];
                    $temp3["roll_no"] = $temp2[11];
                    $temp3["address"] = $temp2[12];
                    $temp3["mobile"] = $temp2[13];
                    $values[] = $temp3;                    
                    
                }
            }
            fclose($handle);

            $is_exist =[];
            foreach($values as $key => $d) {
                if($d["email"] && $d["mobile"] && strlen($d["mobile"]) === 10) {
                    $m = $d['email'];
                    $exist = Student::where('email',$m)->get();
                    $is_exist[$m] = $exist;
                    if(count($exist) > 0) {
                        return $this->sendResultJSON(false, $m.' this is already exist. please change and upload again.');
                    }
                }
            }
            foreach($values as $key => $d) {
                if($d["email"] && $d["mobile"] && strlen($d["mobile"]) === 10) {
                        $student = new Student();
                        $student->name = $d['name'];
             //           $student->photo = $d[1];
                        $student->gender = $d['gender'];
                        $student->father_name = $d['father_name'];
                        $student->mother_name = $d['mother_name'];
                        $student->dob = $d['dob'];
                        $student->religion = $d['religion'];
                        $student->email = $d['email'];
                        $student->addmission_date = $d['addmission_date'];
                        $student->class = $d['class'];
                        $student->section = $d['section'];
                        $student->roll_no = $d['roll_no'];
                        $student->address = $d['address'];
                        $student->mobile = $d['mobile'];
                        $student->save();
                }
            }

            return $this->sendResultJSON(true, 'csv is successfully uploaded');
        } catch (\Exception $e) {
            report($e);
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    private function cleamCsvText($data){
        $data = str_replace("-", "", $data);
        return $data ;
    }

    private function cleamMobile($data){
        $data = trim($data, "");
        $data = str_replace(" ", "", $data);
        $data = str_replace("-", "", $data);
        return $data ;
    }

}
