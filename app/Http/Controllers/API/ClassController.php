<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Standerd;
use App\Models\Subject;
use Validator;
class ClassController extends Controller
{
    public function getAllClasses(Request $request) {
        try {
            $classes;
            if($request->get('class_id')) {
                $classes = Standerd::where('id',$request->get('class_id'))->with('subjects')->first(); 
            }
            else {
                $classes =  Standerd::with('subjects')->get();
            }
            return $this->sendResultJSON(true, '', $classes);
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function addClass(Request $request) {
        try {
            $element_array = array(
                'class' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $class = new Standerd();
            $class->standerd = $request->get('class');
            $class->save();
            return $this->sendResultJSON(true, 'class is added successfully');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function updateClass(Request $request) {
        try {
            $element_array = array(
                'class' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $class = Standerd::where('id',$request->get('class'))->first();

            if($class) {
                $class->standerd = $request->get('class');
                $class->save();
                return $this->sendResultJSON(true, 'class is updated successfully');
            }
            else {
                return $this->sendResultJSON(false, 'Invalid id');

            }
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function deleteClass(Request $request) {
        try {
            $element_array = array(
                'class' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $class = Standerd::where('id',$request->get('class'))->first();

            if($class) {
                $subject = Subject::where('standerd',$request->get('class'))->first();
                if($subject) {
                    return $this->sendResultJSON(false, 'this class is associated with subjects');
                }
                $class->standerd = $request->get('class');
                $class->delete();
                return $this->sendResultJSON(true, 'class is deleted successfully');
            }
            else {
                return $this->sendResultJSON(false, 'Invalid id');

            }
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }
}
