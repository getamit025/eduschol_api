<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    var $success_code = 200;
    public function sendResultJSON($status, $msg = null, $data = array())
    {
        return response()->json(['result' => $status, 'message' => $msg, 'data' => $data], $this->success_code);
    }
}
