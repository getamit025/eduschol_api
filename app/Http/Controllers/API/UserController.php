<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function addUser(Request $request) {
        try {
            $element_array = array(
                'name' => 'required',
                'email' => "required|email|unique:users,email",
                'password'=> "required|min:6|max:15"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }
            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = bcrypt($request->get('password'));
            $user->save();

            return $this->sendResultJSON(true, 'new user is crescted.');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function getUser(Request $request) {
        $users = User::get();
        return $this->sendResultJSON(true, '', $users );
    }

    public function login(Request $request) {
        try {
            $element_array = array(
                'email' => "required",
                'password'=> "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $user = User::where('email', $request->get('email'))->first();

            if ($user && Hash::check($request->get('password'), $user->password)) {
                $user->access_token = generateAccessToken($user);

                return $this->sendResultJSON(true, __("user_logged_in"), $user);
                
            } else {
                return $this->sendResultJSON(false, __("message.invalid_credientials"));
            }

        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }
}
