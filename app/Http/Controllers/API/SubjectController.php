<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Materail;
use Validator;

class SubjectController extends Controller
{
    public function addSubject(Request $request) {
        try {
            $element_array = array(
                'standerd' => "required",
                'subject' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $subject = new Subject();
            $subject->standerd = $request->get('standerd');
            $subject->subject  = $request->get('subject');
            $subject->save();
            return $this->sendResultJSON(true, 'Subject is added successfully'); 
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
            }
    }

    public function getSubject(Request $request) {
        try {
            $subject;
            if($request->get('subject_id')) {
                $subject = Subject::where('id',$request->get('subject_id'))->with('class')->first();
            }
            else {
                $subject = Subject::with('class')->get();
            }
            return $this->sendResultJSON(true, 'get subjects successfully',$subject);
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function updateSubject(Request $request) {
        try {
            $element_array = array(
                'subject_id' => "required",
                'subject' => "required",
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $subject = Subject::where('standerd',$request->get('subject_id'))->first();
            if($subject) {
                $subject->subject = $request->get('subject');
                $subject->save();
                return $this->sendResultJSON(true, 'update subjects successfully');
            }
            else {
                return $this->sendResultJSON(false, 'Invalid subject_id');
            }
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function deleteSubject(Request $request) {
        try {
            $element_array = array(
                'subject_id' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $materail = Material::where('id',$request->get('subject_id'))->first();
            if($materail) {
                return $this->sendResultJSON(false, 'this subject is associated with student material');
            }
            $subject = Subject::where('id',$request->get('subject_id'))->first();
            
            if($subject) {
                $subject->delete();
                return $this->sendResultJSON(true, 'delete subjects successfully');
            }
            else {
                return $this->sendResultJSON(false, 'Invalid subject_id');
            }
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }
}
