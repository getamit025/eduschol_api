<?php

namespace App\Http\Controllers\API;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Materail;
class MaterialController extends Controller
{

    public function getMaterial(Request $request) {
        try {
            $materail;
            if($request->get('material_id')) {
                $materail = Materail::where('id',$request->get('material_id'))->with('classDeail')->with('subjectDeail')->first();
            }
            else {
                $materail = Materail::with('classDeail')->with('subjectDeail')->get();
            }
            return $this->sendResultJSON(true, 'get material successfully',$materail);
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }
    public function addMaterail(Request $request) {
        try {
            $element_array = array(
                'standerd' => "required",
                'subject' => "required",
                'title' => "required",
                'content' => 'required'
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $materail = new Materail();
            $materail->standerd = $request->get('standerd');
            $materail->subject  = $request->get('subject');
            $materail->title  = $request->get('title');
            $materail->content  = $request->get('content');
            $materail->youtube_url  = $request->get('url');
            if($request->file('video')) {
                $t=time();
                $file_name = $t."_".$request->get('subject').".mp4";
                $path = $request->file('video')->move(public_path("/videos/"),$file_name);
                $file_uri = url('/videos/'.$file_name);
                $materail->vedio = $file_uri;
//                return $this->sendResultJSON(true, 'Student created.',$file_uri);
            }
            $materail->save();
            return $this->sendResultJSON(true, 'materail added successfully');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function updateMaterial(Request $request) {
        try {
            $element_array = array(
                'material_id' => "required",
                'standerd' => "required",
                'subject' => "required",
                'title' => "required",
                'content' => 'required'
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $materail = Materail::where('id',$request->get('material_id'))->first();
            $materail->standerd = $request->get('standerd');
            $materail->subject  = $request->get('subject');
            $materail->title  = $request->get('title');
            $materail->content  = $request->get('content');
            $materail->youtube_url  = $request->get('url');
            if($request->file('video')) {
                $t=time();
                $file_name = $t."_".$request->get('subject').".mp4";
                $path = $request->file('video')->move(public_path("/videos/"),$file_name);
                $file_uri = url('/videos/'.$file_name);
                $materail->vedio = $file_uri;
//                return $this->sendResultJSON(true, 'Student created.',$file_uri);
            }
            $materail->save();
            return $this->sendResultJSON(true, 'materail updated successfully');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }

    public function deleteMaterial(Request $request) {
        try {
            $element_array = array(
                'material_id' => "required"
            );
            $validator = Validator::make($request->all(), $element_array);

            if ($validator->fails()) {
                return $this->sendResultJSON(false, $validator->errors()->first());
            }

            $materail = Materail::where('id',$request->get('material_id'))->first();
            $materail->delete();
            return $this->sendResultJSON(true, 'materail deleteed successfully');
        }
        catch (\Exception $e) {
            return $this->sendResultJSON(false, $e->getMessage());
        }
    }
}
