<?php 
use Carbon\Carbon;
function generateAccessToken($data)
{
    $token = json_encode(array('user_id' => $data['id'], 'timestamp' => Carbon::Now()->timestamp));
    return base64_encode(base64_encode($token));
}