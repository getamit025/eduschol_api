<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;
    function class()
    {
        return $this->hasOne('App\Models\Standerd', 'id', 'standerd');
    }
}
