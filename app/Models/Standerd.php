<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Standerd extends Model
{
    use SoftDeletes;
    function subjects()
    {
        return $this->hasMany('App\Models\Subject', 'standerd', 'standerd');
    }
}
