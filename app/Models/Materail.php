<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materail extends Model
{
    use SoftDeletes;
    function classDeail()
    {
        return $this->hasOne('App\Models\Standerd', 'id', 'standerd');
    }

    function subjectDeail()
    {
        return $this->hasOne('App\Models\Subject', 'id', 'subject');
    }
}
