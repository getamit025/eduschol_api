<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materails', function (Blueprint $table) {
            $table->id();
            $table->string('standerd');
            $table->string('subject');
            $table->string('title');
            $table->longtext('content');
            $table->string('youtube_url')->nullable();
            $table->string('vedio')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materails');
    }
}
